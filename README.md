# Rell CI Example

This sample repository shows how to test and build rell code using the Chromia CLI (`chr`) 

## Configure a postgres service and set the db url

```yml
variables:
  POSTGRES_DB: postchain
  POSTGRES_USER: postchain
  POSTGRES_PASSWORD: postchain
  CHR_DB_URL: jdbc:postgresql://postgres/postchain

services:
  - postgres
```

## Run Rell tests on Gitlab CI

Extend the chr image and add the test stage

```yml
stages:
 - test

test:
  image: registry.gitlab.com/chromaway/core-tools/chromia-cli/chr:latest
  stage: test
  script:
    - chr test
```

## Build Rell code into a blockchain configuration

Add artifacts path to gitlab CI and build your project using the `chr` image
```yml
stages:
  - build

build:
  image: registry.gitlab.com/chromaway/core-tools/chromia-cli/chr
  stage: build
  script:
    - chr build
  artifacts:
    when: on_success
    paths:
      - build/*.xml
```
